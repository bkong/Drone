'use strict';


var Drone = require('/home/pi/node_modules/rolling-spider');
var noble = require('noble');
var knownDevices  = [];

         
var ACTIVE = true;
var STEPS = 2;

var d = new Drone('RS_B200011','forceConnect');
var a = new Drone();


	
// Setup basic express server

var express = require('express');
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);


app.use('/all', express.static('public'));
app.get('/', function(req, res){
  res.sendFile(__dirname + '/index2.html');
});

http.listen(1234, function(){
  console.log('Server is running in port : 1234 ');
});





// Chatroom

var numUsers = 0;

io.on('connection', function (socket) {
  var addedUser = false;

  socket.emit('welcome', { message: 'Welcome!', id: socket.id });
  socket.on('i am client', console.log);

   socket.on('move', function (data){
      if(data == "Connect"){
        d.connect(function () {
        d.setup(function () {
          console.log('Configured for Rolling Spider! ', d.name);

          d.flatTrim();
          d.startPing();
          d.flatTrim();

            setTimeout(function () {
              console.log('ready for flight');
              socket.emit('display', 'Connected to ' + d.name);
              ACTIVE = true;
            }, 1000);

            });
        });
      }else if(data == "Takeoff"){
            console.log("takeoff");          
            d.takeOff();
            socket.emit('display', 'TakeOff');
      }else if(data == "Forward"){      
             console.log(data);             
            d.forward({ steps: STEPS });
            cooldown();
      }else if(data == "Backward"){               
             console.log(data);   
             d.backward({ steps: STEPS });
             cooldown();
      }else if(data == "Leftward"){ 
            console.log(data);   
            d.tiltLeft({ steps: STEPS });
            cooldown();              
      }else if(data == "Rightward"){
            console.log(data);       
           d.tiltRight({ steps: STEPS });
           cooldown();           
      }else if(data == "Upward"){
            console.log(data);       
           d.up({ steps: STEPS * 2.5 });
           cooldown();           
      }else if(data == "Downward"){
           console.log(data);             
           d.down({ steps: STEPS * 2.5 });
           cooldown();
      }else if(data == "Leftside"){
            console.log(data);                  
           d.turnLeft({ steps: STEPS });
           cooldown(); 
      }else if(data == "Rightside"){
            console.log(data);                  
           d.turnRight({ steps: STEPS });
           cooldown();
      }else if(data == "Frontflip"){
            console.log(data);                  
            d.frontFlip({ steps: STEPS });
            cooldown();
      }else if(data == "Backflip"){ 
            console.log(data);                 
            d.backFlip({ steps: STEPS });
            cooldown();
      }else if(data == "Leftflip"){ 
            console.log(data);                 
            d.leftFlip({ steps: STEPS });
            cooldown();
      }else if(data == "Rightflip"){
              console.log(data);                  
              d.rightFlip({ steps: STEPS });
              cooldown();
      }else if(data == "Disconnect"){
              d.disconnect();
              socket.emit('display', 'Disconnected');
              setTimeout(function () {
              process.exit();
              }, 3000);
      }else if(data == "Land"){
           console.log('Initiated Landing Sequence...');
            d.land();
      }else if(data == "discover"){
        
        if (noble.state === 'poweredOn') {
          start();      
        } else {
          noble.on('stateChange', start);
        }
      }else if(data == "stop"){
        console.log('stop');
        noble.stopScanning();
      }
     
    });
  // when the client emits 'new message', this listens and executes
  socket.on('new message', function (data) {
    // we tell the client to execute 'new message'
    socket.broadcast.emit('new message', {
      username: socket.username,
      message: data
    });
  });

  // when the client emits 'add user', this listens and executes
  socket.on('add user', function (username) {
    if (addedUser) return;

    // we store the username in the socket session for this client
    socket.username = username;
    ++numUsers;
    addedUser = true;
    socket.emit('login', {
      numUsers: numUsers
    });
    // echo globally (all clients) that a person has connected
    socket.broadcast.emit('user joined', {
      username: socket.username,
      numUsers: numUsers
    });
  });

  // when the client emits 'typing', we broadcast it to others
  socket.on('typing', function () {
    socket.broadcast.emit('typing', {
      username: socket.username
    });
  });

  // when the client emits 'stop typing', we broadcast it to others
  socket.on('stop typing', function () {
    socket.broadcast.emit('stop typing', {
      username: socket.username
    });
  });

  // when the user disconnects.. perform this
  socket.on('disconnect', function () {
    if (addedUser) {
      --numUsers;

      // echo globally that this client has left
      socket.broadcast.emit('user left', {
        username: socket.username,
        numUsers: numUsers
      });
    }
  });
});




function start () {
  // var UUID = ['e014774c3d67','e01476f63d67'];
  noble.startScanning();

  noble.on('discover', function(peripheral) {
    if (!Drone.isDronePeripheral(peripheral)) {
      return; // not a rolling spider
    }

    var details = {
      name: peripheral.advertisement.localName,
      uuid: peripheral.uuid,
      rssi: peripheral.rssi
    };

    knownDevices.push(details);
  
    console.log(knownDevices.length + ': ' + details.name + ' (' + details.uuid + '), RSSI ' + details.rssi);
    // noble.stopScanning();
    io.emit('drone', details);

   

  });

  //  setTimeout(function(){
  //   console.log('test');
  //   noble.stopScanning();

  //   },3000);

  // noble.on('scanStop',function(){
  //   noble.state == 'poweredOff';
  //   console.log("stopping!");
  //   // console.log('Connecting!!!');

  //    d.connect(function () {
  //       d.setup(function () {
  //         console.log('Configured for Rolling Spider! ', d.name);

  //         d.flatTrim();
  //         d.startPing();
  //         d.flatTrim();

  //           setTimeout(function () {
  //             console.log('ready for flight');
  //             d.disconnect();
  //             // socket.emit('display', 'Connected to ' + d.name);
  //             //ACTIVE = true;
  //           }, 1000);

  //           });
  //       });
  //    console.log('wala ka connect');
  // });
     
}

function cooldown() {
  ACTIVE = false;
  setTimeout(function () {
    ACTIVE = true;
  }, STEPS * 12);
}