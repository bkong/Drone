'use strict';


var Drone = require('/home/pi/node_modules/rolling-spider');

var ACTIVE = true;
var STEPS = 2;

var d = new Drone('RS_B200011','forceConnect');

function cooldown() {
  ACTIVE = false;
  setTimeout(function () {
    ACTIVE = true;
  }, STEPS * 12);
}


var http = require('http'),
   fs = require('fs'),
   // NEVER use a Sync function except at start-up!
   index = fs.readFileSync(__dirname + '/simplesocket.html');

// Send index.html to all requests
var app = http.createServer(function(req, res) {
   res.writeHead(200, {'Content-Type': 'text/html'});
   res.end(index);
});

// Socket.io server listens to our app
var io = require('socket.io').listen(app);


// Emit welcome message on connection
io.on('connection', function(socket) {
   // Use socket to communicate with this particular client only, sending it it's own id
   socket.emit('welcome', { message: 'Welcome!', id: socket.id });
   socket.on('i am client', console.log);

   socket.on('move', function(data){

   		if(data == "Connect"){
   			d.connect(function () {
 		 		d.setup(function () {
    			console.log('Configured for Rolling Spider! ', d.name);
    			d.flatTrim();
    			d.startPing();
    			d.flatTrim();

  			    setTimeout(function () {
  			      console.log('ready for flight');
  			      ACTIVE = true;
  			    }, 1000);

  		  		});
  			});
   		}else if(data == "Takeoff"){
         		console.log("takeoff");
			      d.takeOff();
      }else if(data == "Forward"){               
            d.forward({ steps: STEPS });
            cooldown();
      }else if(data == "Backward"){               
             d.backward({ steps: STEPS });
             cooldown();
      }else if(data == "Leftward"){ 
            d.tiltLeft({ steps: STEPS });
            cooldown();              
      }else if(data == "Rightward"){    
           d.tiltRight({ steps: STEPS });
           cooldown();           
      }else if(data == "Upward"){    
           d.up({ steps: STEPS * 2.5 });
           cooldown();           
      }else if(data == "Downward"){               
           d.down({ steps: STEPS * 2.5 });
           cooldown();
      }else if(data == "Leftside"){               
           d.turnLeft({ steps: STEPS });
           cooldown(); 
      }else if(data == "Rightside"){               
           d.turnRight({ steps: STEPS });
           cooldown();
      }else if(data == "Frontflip"){               
            d.frontFlip({ steps: STEPS });
            cooldown();
      }else if(data == "Backflip"){               
            d.backFlip({ steps: STEPS });
            cooldown();
      }else if(data == "Leftflip"){               
            d.leftFlip({ steps: STEPS });
            cooldown();
      }else if(data == "Rightflip"){               
              d.rightFlip({ steps: STEPS });
              cooldown();
      }else if(data == "Disconnect"){
              d.emergency();
              setTimeout(function () {
              process.exit();
              }, 3000);
      }else if(data == "Land"){
           console.log('Initiated Landing Sequence...');
            d.land();
      }


   });
});

app.listen(1234,console.log("Server is running!"));





// make `process.stdin` begin emitting 'keypress' events
// keypress(process.stdin);

// listen for the 'keypress' event


// process.stdin.setRawMode(true);
// process.stdin.resume();

// if (process.env.UUID) {
//   console.log('Searching for ', process.env.UUID);
// }





    // d.on('battery', function () {
    //   console.log('Battery: ' + d.status.battery + '%');
    //   d.signalStrength(function (err, val) {
    //     console.log('Signal: ' + val + 'dBm');
    //   });

    // });

    // d.on('stateChange', function () {
    //   console.log(d.status.flying ? "-- flying" : "-- down");
    // })
//     setTimeout(function () {
//       console.log('ready for flight');
//       ACTIVE = true;
//     }, 1000);

//   });
// });

// process.stdin.on('keypress', function (ch, key) {
//   if (ACTIVE && key) {
//     if (key.name === 'm') {
//       d.emergency();
//       setTimeout(function () {
//         process.exit();
//       }, 3000);
//     } else if (key.name === 't') {
//       console.log('takeoff');
//       d.takeOff();

//     } else if (key.name === 'w') {
//       d.forward({ steps: STEPS });
//       cooldown();
//     } else if (key.name === 's') {
//       d.backward({ steps: STEPS });
//       cooldown();
//     } else if (key.name === 'left') {
//       d.turnLeft({ steps: STEPS });
//       cooldown();
//     } else if (key.name === 'a') {
//       d.tiltLeft({ steps: STEPS });
//       cooldown();
//     } else if (key.name === 'd') {
//       d.tiltRight({ steps: STEPS });
//       cooldown();
//     } else if (key.name === 'right') {
//       d.turnRight({ steps: STEPS });
//       cooldown();
//     } else if (key.name === 'up') {
//       d.up({ steps: STEPS * 2.5 });
//       cooldown();
//     } else if (key.name === 'down') {
//       d.down({ steps: STEPS * 2.5 });
//       cooldown();
//     } else if (key.name === 'i' || key.name === 'f') {
//       d.frontFlip({ steps: STEPS });
//       cooldown();
//     } else if (key.name === 'j') {
//       d.leftFlip({ steps: STEPS });
//       cooldown();
//     } else if (key.name === 'l') {
//       d.rightFlip({ steps: STEPS });
//       cooldown();
//     } else if (key.name === 'k') {
//       d.backFlip({ steps: STEPS });
//       cooldown();
//     } else if (key.name === 'q') {
//       console.log('Initiated Landing Sequence...');
//       d.land();
// //      setTimeout(function () {
// //        process.exit();
// //      }, 3000);
//     }
//   }
//   if (key && key.ctrl && key.name === 'c') {
//     process.stdin.pause();
//     process.exit();
//   }
// });




//launch();
